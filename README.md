# Conversione da MODI XML a tabella CSV

Questo script converte un pacchetto di dati MODI 4.00 in formato XML, così come esportato da SIGECWeb, in una tabella CSV compatibile con le specifiche di inventariazione patrimoniale del Modello 15 C.G. attualmente in uso per Soprintendenze e Poli Museali del Ministero dei Beni e delle Attività Culturali e del Turismo.

È stato creato da Stefano Costa e Barbara Barbaro della SABAP Liguria.