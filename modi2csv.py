#! /usr/bin/env python3

import argparse
import csv
import sys
import tempfile
import xml.etree.ElementTree as ET
import zipfile


fields = (
    'CDR',  # codice regione
    'AMB',  # ambito di catalogazione
    'OGD',  # definizione oggetto
    'QNTN', # quantità
    'STCC', # stato di conservazione
    'INPC', # numero di inventario patrimoniale
    'INPR', # data di immissione nel patrimonio
    'INPD', # codice SEC
    'INPM', # modalità di acquisizione
    'INPP', # provenienza
    'INPA', # valore patrimoniale
    'INPZ', # categoria SEC
    'INPS', # codice meccanografico
    'INPU', # causale
    'MTCM', # materiale
    'CMR',  # funzionario responsabile
    'CMC',  # compilatore
    'OSS',  # note
    )

def flatten(tree, sep=''):
    '''Flatten an XML tree and return the text content of all children.'''
    
    try:
        flat = sep.join(tree.text)
    except TypeError:
        flat = sep.join(flatten(i) for i in tree.getchildren())
    return flat


def modi2dict(datafile):
    '''Convert data from a Zip archive exported from SIGECWeb to a dict.'''

    with zipfile.ZipFile(datafile) as modizip:
        with modizip.open('SS19MODI.xml') as modixml:
            tree = ET.fromstring(modixml.read())
            schede = tree.find('schede').getchildren()
            result = []
            for scheda in schede:
                s_d = dict()
                for f in fields:
                    try:
                        s_d[f] = scheda.find('.//{}'.format(f)).text
                    except AttributeError:
                        s_d[f] = ''
                # tree
                og = scheda.find('.//OG')
                ogfull = ' '.join(i.text for i in og.getchildren() if i.tag.startswith('OG'))
                s_d['OG'] = ogfull
                # tree
                lc = scheda.find('.//LC')
                lcfull = ', '.join(i.text for i in lc.getchildren())
                ldcn = scheda.find('.//LDCN').text
                s_d['LC'] = ', '.join((lcfull, ldcn))
                # flat tree
                s_d['DT'] = flatten(scheda.find('.//DT'), sep=' ')
                # flat tree
                s_d['MIS'] = '; '.join([flatten(i, sep=' ') for i in scheda.findall('.//MIS')])
                result.append(s_d)
            return result

def modidict2csv(modidict, output):
    '''Convert data from a dict to a CSV file.'''

    fieldnames = ['CDR', 'AMB', 'OGD', 'QNTN', 'STCC', 'INPC', 'INPR', 'INPD', 'INPM', 'INPP', 'INPA', 'INPZ', 'INPS', 'INPU', 'MTCM', 'CMR', 'CMC', 'OSS', 'OG', 'LC', 'DT', 'MIS']
    writer = csv.DictWriter(output, fieldnames=fieldnames)

    writer.writeheader()
    writer.writerows(modidict)


def parse_args(args):
    '''Parse command line arguments.

    It lives in its own function as per http://stackoverflow.com/a/18161115'''

    parser = argparse.ArgumentParser(description='Convert MODI XML to CSV.')
    parser.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
    return parser.parse_args(args)


def main():
    '''Main program function.'''

    parser = parse_args(sys.argv[1:])
    # .buffer needed because this is fed into ZipFile()
    imported_data = modi2dict(parser.infile.buffer)
    exported_data = modidict2csv(imported_data, parser.outfile)


if __name__ == '__main__':
    main()
