import unittest

import tempfile

import modi2csv

class ConvertTestCase(unittest.TestCase):
    def test_input_to_dict(self):
        expected_result = [{'AMB': 'archeologico',
 'CDR': '07',
 'CMC': 'Trigona, Simon Luca',
 'CMR': 'Barbaro, Barbara',
 'DT': 'Età romana I a.C.ca',
 'INPA': '€ 300,00',
 'INPC': '17.6.12',
 'INPD': '015  - CERAMICHE DI INTERESSE ARCHEOLOGICO',
 'INPM': 'Rinvenimento fortuito',
 'INPP': 'Santa Margherita Ligure (GE)',
 'INPR': '2017/04/06',
 'INPS': 'BA CA BA AA DA',
 'INPU': 'A66',
 'INPZ': 'BENI ARCHEOLOGICI',
 'LC': 'ITALIA, Liguria, GE, Portofino, PORTOFINO, Via alla Penisola - 16034, '
       'Deposito pesso sede Area Marina Protetta di Portofino',
 'MIS': 'altezza m 1',
 'MTCM': 'Ceramica',
 'OG': 'anfora da trasporto Dressel 1B con bollo',
 'OGD': 'anfora',
 'QNTN': '1',
 'STCC': 'mediocre',
 'OSS': ''}]
        processed_result = modi2csv.modi2dict('data/MODI_4.00_ICCD0_exp_barbara.barbaro1491475641930.zip')
        self.assertEqual(expected_result, processed_result)

    def test_dict_to_output(self):
        test_input = [{'AMB': 'archeologico',
 'CDR': '07',
 'CMC': 'Trigona, Simon Luca',
 'CMR': 'Barbaro, Barbara',
 'DT': 'Età romana I a.C.ca',
 'INPA': '€ 300,00',
 'INPC': '17.6.12',
 'INPD': '015  - CERAMICHE DI INTERESSE ARCHEOLOGICO',
 'INPM': 'Rinvenimento fortuito',
 'INPP': 'Santa Margherita Ligure (GE)',
 'INPR': '2017/04/06',
 'INPS': 'BA CA BA AA DA',
 'INPU': 'A66',
 'INPZ': 'BENI ARCHEOLOGICI',
 'LC': 'ITALIA, Liguria, GE, Portofino, PORTOFINO, Via alla Penisola - 16034, '
       'Deposito pesso sede Area Marina Protetta di Portofino',
 'MIS': 'altezza m 1',
 'MTCM': 'Ceramica',
 'OG': 'anfora da trasporto Dressel 1B con bollo',
 'OGD': 'anfora',
 'QNTN': '1',
 'STCC': 'mediocre',
 'OSS': ''}]
        expected_output = 'CDR,AMB,OGD,QNTN,STCC,INPC,INPR,INPD,INPM,INPP,INPA,INPZ,INPS,INPU,MTCM,CMR,CMC,OSS,OG,LC,DT,MIS\r\n07,archeologico,anfora,1,mediocre,17.6.12,2017/04/06,015  - CERAMICHE DI INTERESSE ARCHEOLOGICO,Rinvenimento fortuito,Santa Margherita Ligure (GE),"€ 300,00",BENI ARCHEOLOGICI,BA CA BA AA DA,A66,Ceramica,"Barbaro, Barbara","Trigona, Simon Luca",,anfora da trasporto Dressel 1B con bollo,"ITALIA, Liguria, GE, Portofino, PORTOFINO, Via alla Penisola - 16034, Deposito pesso sede Area Marina Protetta di Portofino",Età romana I a.C.ca,altezza m 1\r\n'
        with tempfile.TemporaryFile(mode='w+', newline='', encoding='utf-8') as test_output:
            modi2csv.modidict2csv(test_input, test_output)
            test_output.seek(0)
            self.assertEqual(expected_output, test_output.read())

class CommandLine(unittest.TestCase):
    def setUp(self):
        self.parser = modi2csv.parse_args(['data/MODI_4.00_ICCD0_exp_barbara.barbaro1491475641930.zip', 'MODI_4.00_ICCD0_exp_barbara.barbaro1491475641930.csv'])

    def test_parser(self):
        self.assertEqual(self.parser.infile.name, 'data/MODI_4.00_ICCD0_exp_barbara.barbaro1491475641930.zip')
        self.assertEqual(self.parser.infile.mode, 'r')
        self.assertEqual(self.parser.outfile.name, 'MODI_4.00_ICCD0_exp_barbara.barbaro1491475641930.csv')
        self.assertEqual(self.parser.outfile.mode, 'w')

    def tearDown(self):
        self.parser.infile.close()
        self.parser.outfile.close()
